This is a POC to test compliance of infrastructure created with Terraform using inspec

### Pinpoints
- All specific values used in scripts and commands are in awsvar file
- This lab uses inspec in a container from the official Chef image
- main.tf is a simple plan for creating an EC2 instance and a S3 bucket
- inspec profile is located in directory specified in $INSPECDIR variable

### Reproducing steps
1- Edit awsvar file and set variables accordingly

2- Add variables to environment from file using the source command

```
source awsvar
```

3- Create a directory for source code with writing permissions for everyone. This will be mounted as volume in inspec container

```
mkdir $VOLDIR
chmod a+w $VOLDIR
```
4- Pull inspec container image 

```
$CONTAINER_BIN pull chef/inspec
```

5- Generate inspec profile
```
$CONTAINER_BIN run --name terra-poc --rm -v ${VOLDIR}:/src -it chef/inspec init profile --platform aws /src/${INSPECDIR}
```

6- Edit main.tf file if needed to declare the desired state of the infrastructure

7- Apply terraform plan to create the aws infra
```
terraform plan
terraform apply
```

8- Edit the default inspec control to meet the testing requirements, the example in ./my-cloud/controls/example.rb tests for the existance of both the EC2 instance and the S3 bucket and -also- that bucket is not publicly accessible

9- Run inspec for compliance testing
The inspec command is invoked from within a container. However, the resulting command is too big to be handled in the terminal command line, so -for practical reasons- it is contained in a file called `inspecit` for easeness.

```
./inspecit
```

10- The output of the last command should display the result of the tests, review it and repeat steps 8 and 9 as needed for additional tests.

11- When done, destroy the created cloud infra 
```
terraform destroy
```$


